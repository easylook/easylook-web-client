define([
    'lib/backbone',
    'app/model/map/zone'
], function (Backbone, Zone) {
    return Backbone.Collection.extend({
        url: '/api/zone',

        model: Zone,

        getByTrackerId: function(trackerId) {
            return this.findWhere({
                trackerId: trackerId
            });
        },

        getById: function(id) {
            return this.findWhere({
                id: id
            });
        }
    });
});
