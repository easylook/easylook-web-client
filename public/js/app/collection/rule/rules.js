define([
    'lib/backbone',
    'app/model/rule/rule'
], function (Backbone, Rule) {
    return Backbone.Collection.extend({
        url: '/api/tracker-rule',

        model: Rule
    });
});
