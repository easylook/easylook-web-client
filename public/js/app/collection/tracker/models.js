define([
    'lib/backbone',
    'app/model/tracker/model'
], function (Backbone, Model) {
    return Backbone.Collection.extend({
        url: '/api/models',

        model: Model,

        parse: function(data) {
            return data.equipmentModels;
        },

        getById: function(id) {
            return this.findWhere({
                id: id.toString()
            });
        }
    });
});
