define([
    'lib/backbone',
    'app/model/tracker/type'
], function (Backbone, Type) {
    return Backbone.Collection.extend({
        url: '/api/tracker-types',

        model: Type,

        getById: function(id) {
            return this.findWhere({
                id: id.toString()
            });
        },

        parse: function(data) {
            return data.trackerTypes;
        }
    });
});
