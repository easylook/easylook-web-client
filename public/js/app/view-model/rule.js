define([
    'lib/underscore',
    'app/view-model/base',
    'app/collection/map/groups',
    'app/collection/map/zones',
    'app/collection/rule/rules'
], function (_, BaseViewModel, Groups, Zones, Rules) {
    return BaseViewModel.extend({
        defaults: {
            groups: new Groups(),
            zones: new Zones(),
            rules: new Rules()
        },

        initialize: function() {
            var that = this,
                promises;

            BaseViewModel.prototype.initialize.apply(that, arguments);

            promises = {
                groups: that.get('groups').fetch(),
                zones: that.get('zones').fetch(),
                rules: that.get('rules').fetch()
            };

            that.set('promises', promises);
        }
    });
});