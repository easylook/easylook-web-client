define([
    'lib/jquery',
    'app/view/map/zone-context',
    'app/model/map/zone'
], function ($, ZoneContext, Zone) {
    var mapCircle;
    var zoneModel;

    var editableCircle = function (viewModel, zone) {
        var center =[],
            radius = 1000;

        zoneModel = zone || new Zone();

        if (zone) {
            center = zone.get('coordinates')[0];
            radius = zone.get('radius');
        }

        mapCircle = new ymaps.Circle([
            center,
            radius
        ], {}, {
            // Задаем опции круга.
            // Включаем возможность перетаскивания круга.
            draggable: true,
            // Цвет заливки.
            // Последний байт (77) определяет прозрачность.
            // Прозрачность заливки также можно задать используя опцию "fillOpacity".
            fillColor: "#DB709377",
            // Цвет обводки.
            strokeColor: "#990066",
            // Прозрачность обводки.
            strokeOpacity: 0.8,
            // Ширина обводки в пикселях.
            strokeWidth: 5
        });

        var editClicked = false;

        var onMouseMove = function (e) {
            var evX = e.get('globalPixels')[0],
                evY = e.get('globalPixels')[1],
                radius = mapCircle.geometry.getPixelGeometry().getRadius(),
                radiusWithStroke = mapCircle.geometry.getPixelGeometry().getRadius() + 10,
                circleCenterX = mapCircle.geometry.getPixelGeometry().getCoordinates()[0],
                circleCenterY = mapCircle.geometry.getPixelGeometry().getCoordinates()[1],
                eventRadius2 = (evX - circleCenterX) * (evX - circleCenterX) + (evY - circleCenterY) * (evY - circleCenterY);

            if ((eventRadius2 > (radius - 20) * (radius - 20)) && (eventRadius2 < (radiusWithStroke) * (radiusWithStroke))) {
                viewModel.get('map').cursors.push('crosshair');
                mapCircle.options.set('draggable', false);
                if (editClicked) {
                    var circleRadius = mapCircle.geometry.getRadius();
                    mapCircle.geometry.setRadius(parseInt(
                        circleRadius * (Math.sqrt(eventRadius2) / (radius - 5))
                    ));
                }
            } else {
                viewModel.get('map').cursors.push('pointer');
                mapCircle.options.set('draggable', true);
            }
        };

        var onMouseDown = function (e) {
            e.stopPropagation();
            e.preventDefault();
            editClicked = true;
        };

        var onMouseUp = function (e) {
            e.stopPropagation();
            e.preventDefault();
            editClicked = false;
        };

        var onContextMenu = function (e) {
            e.preventDefault();
            e.stopPropagation();
            var position = e.get('position');

            viewModel.set('currentZone', {
                zone: mapCircle,
                zoneModel: zoneModel
            });

            new ZoneContext({
                viewModel: viewModel
            }).render({
                    left: position[0],
                    top: position[1],
                    position: 'absolute'
                });
        };

        mapCircle.events.add('mousemove', onMouseMove);
        mapCircle.events.add(['mousedown', 'dragstart'], onMouseDown);
        mapCircle.events.add(['mouseup', 'dragend'], onMouseUp);
        mapCircle.events.add('contextmenu', onContextMenu);
    };

    editableCircle.prototype = {
        getMapCircle: function (center) {
            if (center) {
                mapCircle.geometry.setCoordinates(center);
            }
            return mapCircle;
        },

        getZoneModel: function () {
            return zoneModel;
        }
    };

    return editableCircle;
});

