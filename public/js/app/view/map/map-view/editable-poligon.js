define([
    'lib/jquery',
    'app/view/map/zone-context',
    'app/model/map/zone'
], function ($, ZoneContext, Zone) {
    var mapPolygon;
    var zoneModel;

    var editablePolygon = function (viewModel, model) {
        var coordinates = [];
        zoneModel = model || new Zone();
        if (model) {
            coordinates[0] = model.get('coordinates');
        }

        mapPolygon = new ymaps.Polygon(coordinates, {}, {
            // Курсор в режиме добавления новых вершин.
            editorDrawingCursor: "crosshair",
            // Максимально допустимое количество вершин.
            editorMaxPoints: 5,
            // Цвет заливки.
            fillColor: 'rgba(242,112,147, 0.5)',
            // Цвет обводки.
            strokeColor: '#990066',
            // Ширина обводки.
            strokeWidth: 5
        });

        var onContextMenu = function (e) {
            e.preventDefault();
            e.stopPropagation();
            var position = e.get('position');

            viewModel.set('currentZone', {
                zone: mapPolygon,
                zoneModel: zoneModel
            });

            new ZoneContext({
                viewModel: viewModel
            }).render({
                    left: position[0],
                    top: position[1],
                    position: 'absolute'
                });
        };

        mapPolygon.events.add('contextmenu', onContextMenu);

        var stateMonitor = new ymaps.Monitor(mapPolygon.editor.state);

        stateMonitor.add("drawing", function (newValue) {
            mapPolygon.options.set("strokeColor", newValue ? '#FF0000' : '#0000FF');
        });

    };

    editablePolygon.prototype = {
        getMapPolygon: function () {
            return mapPolygon;
        },

        startDrawing: function () {
            mapPolygon.editor.startDrawing();
        },

        getZoneModel: function() {
            return zoneModel;
        }
    };

    return editablePolygon;
});


