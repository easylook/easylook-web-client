define([
    'lib/underscore',
    'lib/jquery',
    'lib/handlebars',
    'app/view/shared/base',
    'text!app/template/map/tracker-condition.html',
    'text!app/template/map/li-tracker-additional.html'
], function (_, $, Handlebars, BaseView, trackerCondition, trackerAditional) {
    return BaseView.extend({
        className: 'tracker-block',
        template: Handlebars.compile(trackerCondition),

        initialize: function () {
            var that = this;

            BaseView.prototype.initialize.apply(that, arguments);
            that.listenTo(that.viewModel, 'change:currentTracker', that.changeCurrentTrackerInfo);
            that.listenTo(that.viewModel, 'change:currentTrackerData', that.updateTrackerInfo);
        },

        changeCurrentTrackerInfo: function () {
            var model = this.viewModel.get('currentTracker');

            if (model) {
                this.$el.addClass('active');
                var trackerData = this.viewModel.get('periodic').getDataByTrackerId(
                    parseInt(model.get('id')));
                this.viewModel.set('currentTrackerData', trackerData);
            } else {
                this.$el.removeClass('active');
            }
        },

        updateTrackerInfo: function () {
            var currentTrackerData = this.viewModel.get('currentTrackerData');

            if (!currentTrackerData) {
                return;
            }

            var $additionalContainer = this.$('.tracker-additional');

            this.$('.tracker-info-lat').html(currentTrackerData.get('lat').toFixed(4));
            this.$('.tracker-info-lng').html(currentTrackerData.get('lng').toFixed(4));
            this.$('.tracker-info-speed').html(currentTrackerData.get('speed').toFixed(2));
            var startDate = this.getDateObject(currentTrackerData.get('logDate'));
            this.$('.tracker-info-last-date').html(startDate.hours + ':' + startDate.min);
            this.$('.tracker-info-direction').html(currentTrackerData.get('direction'));

            if (currentTrackerData.get('dataSource') && !_.isNull(currentTrackerData.get('dataSource'))) {
                this.$('.tracker-info-row.source').show();
                this.$('.tracker-info-source').html(currentTrackerData.get('dataSource'));
            } else {
                this.$('.tracker-info-row.source').hide();
            }

            var status = currentTrackerData.get('trackerStatus'),
                placeMark = $('.map-icon[data-id=' + currentTrackerData.get('trackerId') +']');

            this.$('.tracker-info-status').html(status == 'in_place' ? 'На месте' : 'В движении');

            if (placeMark) {
                if (status == 'in_place') {
                    placeMark.removeClass('in-motion');
                } else {
                    placeMark.addClass('in-motion');
                }
            }

            var params = currentTrackerData.get('params').params;
            $additionalContainer.empty();
            if (params) {
                _.each(params, function(param) {
                    $additionalContainer.append(
                        Handlebars.compile(trackerAditional)({
                            name: param.trackerParam.name,
                            value: param.value
                        })
                    );
                })
            }
        }
    });
});