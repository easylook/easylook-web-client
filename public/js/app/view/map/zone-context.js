define([
    'lib/jquery',
    'lib/handlebars',
    'app/view/shared/base',
    'app/model/map/zone',
    'text!app/template/map/zone-context.html'
], function ($, Handlebars, BaseView, Zone, zoneContext) {
    return BaseView.extend({

        className: 'dropdown',

        template: Handlebars.compile(zoneContext),

        events: {
            'click .save-zone' : 'saveZone'
        },

        initialize: function () {
            BaseView.prototype.initialize.apply(this, arguments);
            this.$container = $('body');
            this.listenTo(this.viewModel, 'mapAction', this.remove);
        },

        render: function (position) {
            BaseView.prototype.render.apply(this, arguments);
            $('.dropdown').remove();
            this.$el.css(position);

            this.$container.append(this.$el);

            return this;
        },

        saveZone: function () {
            var that = this,
                zone = that.viewModel.get('currentZone').zone,
                zoneModel = that.viewModel.get('currentZone').zoneModel,
                data = {};

            if (zoneModel.isNew()) {
                zoneModel.set('title', 'Новая зона');
            }

            if (zone.geometry.getType() == 'Circle') {
                data = {
                    userId: window.serverData.userId,
                    type: 2,
                    radius: zone.geometry.getRadius(),
                    coordinates: [zone.geometry.getCenter()]
                };
            } else {
                data = {
                    userId: window.serverData.userId,
                    type: 1,
                    title: 'Новая зона',
                    coordinates: zone.geometry.getCoordinates()[0]
                };
            }

            zoneModel.save(data, {
                success: function () {
                    that.viewModel.get('zones').fetch();

                },
                error: function () {
                    console.log('error');
                }
            });

            this.remove();
        }
    });
});