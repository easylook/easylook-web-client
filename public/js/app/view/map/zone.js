define([
    'lib/jquery',
    'lib/handlebars',
    'app/view/shared/base',
    'app/view/map/map-view/editable-circle',
    'app/view/map/map-view/editable-poligon',
    'text!app/template/map/zone.html'
], function ($, Handlebars, BaseView, EditableCircle, EditablePolygon, zone) {
    return BaseView.extend({
        className: 'zone',

        events: {
            'click': 'showZone',
            'click .zone-edit': 'editZone',
            'click .zone-remove': 'removeRemove',
            'click .zone-edit-title': 'stopPropagation'
        },

        template: Handlebars.compile(zone),

        showZone: function () {
            var map = this.viewModel.get('map');
            if (this.model.get('type') == 1) {
                var editablePolygon = new EditablePolygon(this.viewModel, this.model);
                map.geoObjects.add(editablePolygon.getMapPolygon());
                this.viewModel.get('zoneObjects').push(editablePolygon.getMapPolygon());
                editablePolygon.startDrawing();
            } else {
                var editableCircleModel = new EditableCircle(this.viewModel, this.model);
                map.geoObjects.add(editableCircleModel.getMapCircle());
                this.viewModel.get('zoneObjects').push(editableCircleModel.getMapCircle());
            }
        },

        editZone: function (e) {
            e.stopPropagation();
            var that = this;
            var $input = that.$('.zone-edit-title'),
                $title = that.$('.zone-item-title');

            if ($input.is(':visible')) {
                that.model.set('title', $input.val());
                $input.hide();
                $title.show();
                that.model.save({}, {
                    success: function () {
                        $title.html($input.val());
                    }
                });
            } else {
                $input.show();
                $title.hide();
            }

        },

        stopPropagation: function(e) {
            e.stopPropagation();
        },

        removeRemove: function (e) {
            e.stopPropagation();
            var result = confirm('Вы действительно хотите удалить эту зону?');
            if (result) {
                var that = this;
                that.model.destroy({
                    wait: true,
                    success: function () {
                        that.viewModel.get('zones').fetch();
                    }
                });
            }
        }
    });
});
