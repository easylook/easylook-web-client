define([
    'lib/jquery',
    'lib/underscore',
    'lib/handlebars',
    'app/view/shared/base-collection',
    'app/view/map/zone',
    'app/view/map/map-view/editable-circle',
    'app/view/map/map-view/editable-poligon',
    'text!app/template/map/zones.html'
], function ($, _, Handlebars, BaseCollectionView, ZoneView, EditableCircle, EditablePolygon, zoneList) {
    return BaseCollectionView.extend({
        el: '#zone-wrapper',

        events: {
            'click .zone-title': 'toggle',
            'click .circle-zone': 'createCircle',
            'click .rect-zone': 'createRect',
            'click .cancel-zone': 'cancelDrawing'
        },

        template: Handlebars.compile(zoneList),

        itemView: ZoneView,

        initialize: function () {
            BaseCollectionView.prototype.initialize.apply(this, arguments);
            this.viewModel.set('zoneObjects', []);
            this.listenTo(this.collection, 'reset sort sync', this.renderChildren);
        },

        toggle: function(e) {
            var $clickedEl = $(e.target);
            $clickedEl.parent().toggleClass('active', !$clickedEl.parent().hasClass('active'));
        },

        createCircle: function (e) {
            var map = this.viewModel.get('map');
            var editableCircleModel = new EditableCircle(this.viewModel).getMapCircle(map.getCenter());
            this.viewModel.get('zoneObjects').push(editableCircleModel);
            map.geoObjects.add(editableCircleModel);
        },

        createRect: function (e) {
            var map = this.viewModel.get('map');
            var editablePolygon = new EditablePolygon(this.viewModel);
            map.geoObjects.add(editablePolygon.getMapPolygon());
            this.viewModel.get('zoneObjects').push(editablePolygon.getMapPolygon());
            editablePolygon.startDrawing();
        },

        cancelDrawing: function (e) {
            var map = this.viewModel.get('map');
            _.each(this.viewModel.get('zoneObjects'), function(obj){
                map.geoObjects.remove(obj);
            });

        }
    });
});
