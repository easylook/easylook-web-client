define([
    'lib/jquery',
    'lib/handlebars',
    'app/view/shared/base-collection',
    'app/view/rule/rule',
    'app/model/rule/rule',
    'text!app/template/rule/rule-list.html'
], function ($, Handlebars, BaseCollectionView, RuleView, Rule, ruleList) {
    return BaseCollectionView.extend({
        el: '#profile-info',
        template: Handlebars.compile(ruleList),
        itemView: RuleView,

        events: {
            'click .add-rule': 'addRule'
        },

        initialize: function () {

            var that = this;
            BaseCollectionView.prototype.initialize.apply(that, arguments);

            that.config.zeroState = '<div class="empty-rule-view">У вас пока нет ни одного правила</div>';

            that.listenTo(that.collection, 'add', that.appendChild);
            that.listenTo(that.collection, 'remove', that.removeChild);
        },

        addRule: function () {
            var that = this;

            var rule = new Rule({
                title: "Новое правило",
                zoneId: that.viewModel.get('zones').at(0).get('id'),
                trackerId: that.viewModel.get('groups').at(0).get('trackers').at(0).get('id'),
                type: 1,
                userId: window.serverData.userId
            });

            rule.save({}, {
                success: function () {
                    console.log('success');
                    that.viewModel.get('rules').add(rule);
                },
                error: function () {
                    console.log('error');
                }
            });

        }
    });
});
