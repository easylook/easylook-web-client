define([
    'lib/jquery',
    'lib/handlebars',
    'app/view/shared/base',
    'text!app/template/rule/rule.html'
], function ($, Handlebars, BaseView, rule) {
    return BaseView.extend({
        className: 'rule',
        template: Handlebars.compile(rule),
        edit: false,

        events: {
            'click .rule-values .edit-icon': 'editRule',
            'click .rule-edit .edit-icon': 'saveRule',
            'click .rule-values .delete-icon': 'removeModel',
            'click .rule-edit .delete-icon': 'cancelEdit'
        },

        templateData: function () {
            var data = BaseView.prototype.templateData.apply(this, arguments),
                trackers = [],
                groups = this.viewModel.get('groups'),
                trackerModel = groups.getTrackerById(this.model.get('trackerId')),
                zoneModel = this.viewModel.get('zones').getById(this.model.get('zoneId')),
                type = (this.model.get('type') == 1) ? "Выход из зоны" : "Вход в зону";

            _.each(groups.models, function (item) {
                _.each(item.get('trackers').models, function (tracker) {
                    trackers.push({
                        id: tracker.get('id'),
                        title: tracker.get('title')
                    });
                });
            });


            return _.extend(data, {
                typeValue: type,
                zone: zoneModel.get('title'),
                tracker: trackerModel.get('title'),
                zones: this.viewModel.get('zones').toJSON(),
                trackers: trackers,
                types: [
                    {
                        id: 1,
                        title: "Выход из зоны"
                    },
                    {
                        id: 2,
                        title: "Вход в зону"
                    }
                ]
            });
        },

        saveRule: function () {
            var that = this,
                title = that.$('.rule-title-edit').val(),
                trackerId = that.$('.rule-tracker-edit').val(),
                zoneId = that.$('.rule-zone-edit').val(),
                type = that.$('.rule-type-edit').val();

            this.model.save({
                title: title,
                trackerId: trackerId,
                zoneId: zoneId,
                type: type
            }, {
                success: function () {
                    that.render();
                    that.edit = false;
                },
                error: function () {
                    console.log('error');
                }
            });
        },

        editRule: function () {
            if (!this.edit) {
                this.$('.rule-values').hide();
                this.$('.rule-edit').show();
                this.edit = true;
            }
        },

        cancelEdit: function () {
            if (this.edit) {
                this.$('.rule-values').show();
                this.$('.rule-edit').hide();
                this.edit = false;
            }
        },

        removeModel: function () {
            var that = this,
                result = confirm('Вы действительно хотите удалить это правило?');

            if (result) {
                that.model.destroy({
                    wait: true,
                    success: function () {}
                });
            }
        }
    });
});
