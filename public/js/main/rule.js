require([
    'lib/jquery',
    'lib/backbone',
    'app/view-model/rule',
    'app/view/shared/header',
    'app/view/rule/rule-wrapper'
], function ($, Backbone, RuleViewModel, HeaderView, RuleWrapperView) {
    var ruleViewModel = new RuleViewModel();

    new HeaderView({
        viewModel: ruleViewModel
    }).render();

    $.when(
        ruleViewModel.get('promises').groups,
        ruleViewModel.get('promises').rules,
        ruleViewModel.get('promises').zones
    ).done(function() {
            new RuleWrapperView({
                collection: ruleViewModel.get('rules'),
                viewModel: ruleViewModel
            }).render();
        }
    );
});